package com.weixun.cms.service;

import com.jfinal.plugin.activerecord.Db;
import com.weixun.model.CmsChannelArticle;

import java.util.List;


/**
 * 文章栏目关联类
 */
public class ChannelArticleService {
    /**
     * 根据文章id查询该文章所在的栏目id
     */
    public List<CmsChannelArticle> findList(String fk_article_pk,String fk_channel_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from cms_channel_article where 1=1 ");
        if (fk_article_pk != null && !fk_article_pk.equals(""))
        {
            stringBuffer.append(" and fk_article_pk = "+fk_article_pk);
        }
        if (fk_channel_pk != null && !fk_channel_pk.equals(""))
        {
            stringBuffer.append(" and fk_channel_pk = "+fk_channel_pk);
        }
        List<CmsChannelArticle> list = CmsChannelArticle.dao.find(stringBuffer.toString());
        return list;
    }

    public int delete(String ids)
    {
        String sql = "delete from cms_channel_article where  fk_article_pk in("+ids+")";
        Integer result = Db.use("datasource").update(sql);
        return result;
    }
}
