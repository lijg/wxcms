<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">


    <title>后台管理系统</title>
    <% String path = request.getContextPath(); %>
    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css" media="all">

</head>
<body>
<div class="container">

    <form id="linkForm" class="layui-form" method="post" action="" enctype="multipart/form-data">
           <input id="msg_pk" name="msg_pk" type="hidden" />
           <div class="layui-form-item" style="margin: 5% auto">
               <label class="layui-form-label">姓名</label>
               <div class="layui-input-block">
                   <input id="msg_user" name="msg_user"  autocomplete="off" class="layui-input" type="text"/>
               </div>
           </div>

            <div class="layui-form-item">
                <label class="layui-form-label">电话</label>
                <div class="layui-input-block">
                    <input id="msg_phone" name="msg_phone"  autocomplete="off" class="layui-input" type="text"/>
                </div>
            </div>

           <div class="layui-form-item">
               <label class="layui-form-label">地址</label>
               <div class="layui-input-block">
                   <input id="msg_address" name="msg_address" lay-verify="required" autocomplete="off"  class="layui-input" type="text">
               </div>
           </div>

        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-block">
                <input id="msg_title" name="msg_title"  autocomplete="off" class="layui-input" type="text"/>
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">内容</label>
            <div class="layui-input-block">
                <textarea id="msg_content" name="msg_content" class="layui-textarea"></textarea>
            </div>
        </div>
        


           <%--<div class="layui-form-item">--%>
               <%--<div align="center" class="layui-input-block" style="margin: 5% auto">--%>
                   <%--<button class="layui-btn layui-btn-small" align="center" id="edit1" onclick="edit()">保存</button>--%>
                   <%--<button class="layui-btn layui-btn-small" id="reset" align="center" type="reset">重置</button>--%>
               <%--</div>--%>
           <%--</div>--%>

    </form>
</div>
</body>


<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<%--layui--%>
<script>


    layui.use(['form','laydate','upload'],function () {
        var $ = layui.jquery;
        var form = layui.form;
        var layer = layui.layer;
        var upload = layui.upload; // 获取upload模块


        $(function (){
            debugger;
            var msg_pk=$("#msg_pk").val();
            if(msg_pk!=null && msg_pk!=''){
               $.ajax({
                   type:'post',
                   url:"<%=path%>/msg/list",
                   data:{msg_pk:msg_pk},
                   success:function (response) {
                       var json = eval(response); // 数组
                       $.each(json,function (i) {
                           $("#msg_pk").val(json[i].msg_pk);
                           $("#msg_user").val(json[i].msg_user);
                           $("#msg_phone").val(json[i].msg_phone);
                           $("#msg_address").val(json[i].msg_address);
                           $("#msg_title").val(json[i].msg_title);
                           $("#msg_content").html(json[i].msg_content);
                       });
                   }
               });
            }
        });



    })

</script>
</html>
