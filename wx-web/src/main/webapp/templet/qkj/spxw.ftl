<div class="layui-row">
    <div class="layui-col-md12 layui-col-space15">
<#if articlelist ? exists>
    <#list articlelist as article>
        <#if article_index lte 3>
        <li class="layui-col-md3">
            <div class="fly-panel">
                <a class="fly-case-img" href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">
                    <img src="${article.article_titleimg}" alt="${article.article_title}">
                    <cite class="layui-btn layui-btn-primary layui-btn-small">去围观</cite>
                </a>
                <p style="text-align: center"><a href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">${article.article_title}</a></p>
            </div>
        </li>
        </#if>
    </#list>
</#if>
    </div>
</div>
<div class="layui-row">
    <div class="layui-col-md12 layui-col-space15">
    <#if articlelist ? exists>
        <#list articlelist as article>
            <#if article_index gte 4>
                <li class="layui-col-md3">
                    <div class="fly-panel">
                        <a class="fly-case-img" href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">
                            <img src="${article.article_titleimg}" alt="${article.article_title}">
                            <cite class="layui-btn layui-btn-primary layui-btn-small">去围观</cite>
                        </a>
                        <p style="text-align: center"><a href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">${article.article_title}</a></p>
                    </div>
                </li>
            </#if>
        </#list>
    </#if>
    </div>
</div>